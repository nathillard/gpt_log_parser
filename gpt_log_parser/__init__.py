import csv
import json
import sys
from dataclasses import asdict, dataclass
from datetime import datetime
from importlib.resources import files
from pathlib import Path
from typing import List

from gpt_log_parser.models import Message, NodeInfo, Response


@dataclass
class ChatSummary:
    '''
    An abbreviated summary of a chat message containing key information for export
    '''
    role: str
    create_time: int
    message: str
    elapsed: datetime | None = None

def reconstruct_chat(mapping: dict[str, NodeInfo], start_node: str) -> list[str]:
    '''
    Reconstruct the chat history by traversing the tree of nodes

    :param mapping: The mapping of node ids to node info
    :param start_node: The id of the start node
    :return: A list of messages in the order they were sent
    '''
    chat_history = []

    def traverse(node_id):
        node = mapping[node_id]
        message = node.message
        if message is not None:
            chat_history.append(message)
        if node.parent is not None:
            traverse(node.parent)

    traverse(start_node)
    return chat_history[::-1]  # Reverse the list to get the correct order

def create_chat_summary_list(response: Response, chat_history: List[Message]) -> List[ChatSummary]:
    '''
    Create a list of ChatSummary objects from the chat history

    :param response: A response object
    :param chat_history: A list of messages in the order they were sent
    :return: A list of summary objects
    '''
    summary = []
    next_time = None
    for message in reversed(chat_history):
        message_text = message.content.parts[0] if message.content.parts is not None else message.content.text
        record = ChatSummary(
            role=message.author.role,
            create_time=message.create_time.timestamp() if message.create_time is not None else response.create_time.timestamp(),
            message=message_text,
        )
        if next_time is None:
            record.elapsed = None
        else:
            record.elapsed = next_time - record.create_time
        next_time = record.create_time
        summary.append(record)
    return list(reversed(summary))

def print_summary_csv(i: int, chat_summary_list: List[ChatSummary], dict_writer: csv.DictWriter) -> None:
    '''
    Print the summary objects as a CSV

    :param i: The index of the conversation
    :param chat_summary_list: A list of summary objects
    :param dict_writer: A CSV writer
    :return: None
    '''
    for summary in chat_summary_list:
        vals = asdict(summary)
        vals["id"] = i
        dict_writer.writerow(vals)

def main():
    if not len(sys.argv) == 2:
        print("Usage: python -m gpt_log_parser <path_to_parse>")
        sys.exit(1)
    
    path_to_parse = sys.argv[1]
    conversation_file_text = Path(path_to_parse).read_text(encoding="utf-8")
    conversations = json.loads(conversation_file_text)
    
    keys = None
    dict_writer = None

    for i, conversation in enumerate(conversations):
        response = Response(**conversation)
        chat_history = reconstruct_chat(response.mapping, response.current_node)
        chat_summary_list = create_chat_summary_list(response=response, chat_history=chat_history)

        if i == 0:
            keys = list(asdict(chat_summary_list[0])) + ["id"]
            dict_writer = csv.DictWriter(sys.stdout, keys, delimiter="\t")
            dict_writer.writeheader()

        print_summary_csv(response.id, chat_summary_list, dict_writer)

if __name__ == "__main__":
    main()