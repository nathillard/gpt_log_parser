from dataclasses import dataclass
from datetime import datetime

@dataclass
class Author:
    '''
    The author of a message
    '''
    role: str
    metadata: dict[str, any]
    name: str | None = None


@dataclass
class Content:
    '''
    The content of a message
    '''
    content_type: str
    parts: list[any] | None = None
    text: str | None = None


@dataclass
class Message:
    '''
    A message sent by an author
    '''
    id: str
    author: Author
    content: Content
    weight: int
    metadata: dict[str, any]
    recipient: str
    create_time: datetime | None = None
    update_time: datetime | None = None
    end_turn: bool | None = None

    def __post_init__(self):
        self.content = Content(**self.content)
        self.author = Author(**self.author)
        if self.create_time:
            self.create_time = datetime.fromtimestamp(self.create_time)
        if self.update_time:
            self.update_time = datetime.fromtimestamp(self.update_time)

@dataclass
class NodeInfo:
    '''
    Information about a node in the tree
    '''
    id: str
    children: list[str]
    message: Message | None = None
    parent: str | None = None

    def __post_init__(self):
        if self.message:
            self.message = Message(**self.message)

@dataclass
class Response:
    '''
    A response from the API
    '''
    title: str
    create_time: datetime
    update_time: datetime
    mapping: dict[str, NodeInfo]
    moderation_results: list[any]
    current_node: str
    plugin_ids: list[str]
    id: str

    def __post_init__(self):
        for key, value in self.mapping.items():
            self.mapping[key] = NodeInfo(**value)
        if self.create_time:
            self.create_time = datetime.fromtimestamp(self.create_time)
        if self.update_time:
            self.update_time = datetime.fromtimestamp(self.update_time)

# The top-level structure is a list of Response objects
Responses = list[Response]