# README

## Description

A utility for parsing the output of a ChatGPT chat export for further analysis.

## Why?

ChatGPT stores a lot of valuable metadata about the conversations you have with it, such as the underlying representation of the data, timestamps of replies, etc. This utility parses that data into a TSV file that can be imported into a spreadsheet program like Excel or Google Sheets for further analysis.

## Exporting Data

To export data from ChatGPT, do the following:

1. Go to <https://chat.openai.com/>
2. Click on your profile picture in the bottom left
3. Click on "settings", then activate the "Data controls" tab
4. Click "Export" under "Export data", and then "Confirm export"
5. The export will be emailed to your email address on file
6. Download the export and unzip it
7. Within the expanded directory is a file called `conversations.json`. This is the file you will need to parse.

## Setup

1. Install [Poetry](https://python-poetry.org/docs/#installation)
2. Run `poetry install`
3. Run `poetry run parse <filename> > output.tsv`

## Input Format

See [sample_conversations.json](sample_conversations.json) for an example of the input format. This is the format of the `conversations.json` file that is exported from ChatGPT and sent to your email address.

It is based on a tree structure that relates grouped conversations together in a way that corresponds to the tabs in ChatGPT.

## Output Format

See [sample_output.tsv](sample_output.tsv) for an example of the output format. This is a TSV file that can be imported into a spreadsheet program like Excel or Google Sheets.

It has four columns:

1. `role` - the user role of the message
2. `create_time` - the user-readable string of the time the message was sent
3. `message` - the message itself
4. `elapsed` - the amount of time this step took to run, as measured by the difference betwee this message and the following one. This is useful for determining how long it took ChatGPT to respond to a message.
5. `id` - the id of the conversation - this is useful for grouping messages by conversation
